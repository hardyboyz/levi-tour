
    </section>
     <!-- MENU SECTION END-->
    <div class="content-wrapper">
	  
	 
		<div class="container">
					 
			<div class="row">

				<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:60px;margin-bottom:20px">
				
				@foreach ($gallery as $file)
				  <div class="col-sm-4" style="padding-bottom:4px">
				  <a rel="levi-tour[gallery]" title="{{$file->getFileName()}}" href="{{$file->getPathName()}}">
					<img alt="{{$file->getFileName()}}" src="{{$file->getPathName()}}" class="img-responsive"/>
				  </a>   
				  </div>
				 @endforeach
				  
				</div>
			</div>
		</div>
		
	</div>
	
	</section>
	
	<script type="text/javascript" charset="utf-8">
	   $(document).ready(function(){
		 $("a[rel^='levi-tour']").prettyPhoto();
	   });
	 </script>