
<div class="m-b-md">
  <h3 class="m-b-none">Edit</h3>
</div>

  {{ Helper::bootstrap_alert() }}

  {!! Form::open(array('route' => array('frontend.update', $frontend->id), 'method' => 'PUT','enctype'=>'multipart/form-data')) !!}
  <section class="panel panel-default">
    <div class="panel-body">

		
        <div class="form-group">
          <label>Title</label>
          {!! Form::text('title', $frontend->title, array('class' => 'form-control')) !!}
        </div>
		
		@if($frontend->type == 'package')
		<div class="form-group">
          <label>Short Details</label>
          {!! Form::text('short_details', $frontend->short_details, array('class' => 'form-control')) !!}
        </div>
		<div class="form-group">
          <label>Price</label>
          {!! Form::text('price', $frontend->price, array('class' => 'form-control')) !!}
        </div>
		@endif
		
		@if($frontend->type != 'promotion-image')
        <div class="form-group">
          <label>Description</label>
          {!! Form::textarea('description', $frontend->description, array('class' => 'form-control textarea')) !!}
        </div>
		@endif
		
		@if($frontend->type != 'scroller')
		<div class="form-group">
          <label>Image Thumbnail</label>
          {!! Form::file('image', '', array('class' => 'form-control')) !!}
        </div>
		@endif
		
		<div class="form-group">
          <label>Status</label>
          {!! Form::select('status', $status, $frontend->status,array('class' => 'form-control')) !!}
        </div>
		
		<div class="form-group">
          <label>Type : {{ $frontend->type }}</label>
        </div>
       
        
        <div class="line line-dashed line-lg pull-in"></div>
        <div class="pull-right">
          <a href="{{ route('frontend.list') }}" id="btn-user-add" class="btn btn-default btn-sm">Cancel</a>
          <button type="submit" class="btn btn-sm btn-primary">Update</button>
        </div>
    </div>

  </section>
  {!! Form::close() !!}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js"></script>
  <script type="text/javascript">
  tinymce.init({
  selector: 'textarea',
  height: 200,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
  ],
  toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
  </script>