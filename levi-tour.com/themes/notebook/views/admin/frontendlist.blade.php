<div class="m-b-md">
</div>

{{ Helper::bootstrap_alert() }}

<section class="panel panel-default">
	<div class="table-responsive">
		<table class="table table-striped">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>Title</th>
	          <th>Description </th>
	          <th>Status</th>
			  <th>Type</th>
	          <th class="col-md-2 text-center">Action</th>
	        </tr>
	      </thead>
	      <tbody>
	      	@foreach ($frontend as $key => $user)
	        <tr>
	          <th scope="row">{{ $key+1 }}</th>
	          <td>{{ $user->title }}</td>
	          <td>{!! substr($user->description,0,150).'...' !!}</td>
	          <td>{{ $user->status }} </td>
			  <td>{{ $user->type }} </td>
	          <td class="text-center">
                <a data-id="13" class="btn btn-xs btn-default btn-user-edit" href="{{ route('frontend.edit', $user->id) }}"><i class="fa fa-pencil fa-fw"></i></a>
	          </td>
	        </tr>
	        @endforeach
	      </tbody>
	    </table>
	</div>
</section>