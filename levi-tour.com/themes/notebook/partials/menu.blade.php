<aside class="bg-dark lter aside-md hidden-print" id="nav">
  <section class="vbox">
    <section class="w-f scrollable">
      <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">
        
        <nav class="nav-primary hidden-xs">
          <ul class="nav">
            
            <li class="{{ Helper::set_active('home') }}">
              <a href="{{ route('_home') }}" class="{{ Helper::set_active('home.index') }}">
                <i class="fa fa-dashboard icon">
                  <b class="bg-danger"></b>
                </i>
                <span>Home</span>
              </a>
            </li>

            <li class="{{ Helper::set_active('admin') }}">
              <a href="#list"  >
                <i class="fa fa-question-circle icon">
                  <b class="bg-warning"></b>
                </i>
                <span class="pull-right">
                  <i class="fa fa-angle-down text"></i>
                  <i class="fa fa-angle-up text-active"></i>
                </span>
                <span>Menu</span>
              </a>
              <ul class="nav lt">
				<li class="{{ Helper::set_active('admin.frontend') }}">
                  <a href="{{ route('frontend.list') }}">
                    <i class="fa fa-angle-right"></i>
                    <span>Frontend</span>
                  </a>
                </li>
				
                <li class="{{ Helper::set_active('admin.dataPaket') }}">
                  <a href="{{ route('datapaket') }}">
                    <i class="fa fa-angle-right"></i>
                    <span>Data Paket</span>
                  </a>
                </li>
              </ul>
            </li>
			
			
			<li class="{{ Helper::set_active('reservation') }}">
              <a href="#list"  >
                <i class="fa fa-question-circle icon">
                  <b class="bg-warning"></b>
                </i>
                <span class="pull-right">
                  <i class="fa fa-angle-down text"></i>
                  <i class="fa fa-angle-up text-active"></i>
                </span>
                <span>Reservation</span>
              </a>
              <ul class="nav lt">
				<li class="{{ Helper::set_active('reservation.list') }}">
                  <a href="{{ route('reservation.list') }}">
                    <i class="fa fa-angle-right"></i>
                    <span>Reservation List</span>
                  </a>
                </li>
				
				<li class="{{ Helper::set_active('reservation.invoice') }}">
                  <a href="{{ route('invoice.list') }}">
                    <i class="fa fa-angle-right"></i>
                    <span>Invoice</span>
                  </a>
                </li>
				
				<!--<li class="{{ Helper::set_active('reservation.payment') }}">
                  <a href="{{ route('payment.list') }}">
                    <i class="fa fa-angle-right"></i>
                    <span>Payment</span>
                  </a>
                </li>-->
              </ul>
            </li>

            <li class="{{ Helper::set_active('user') }}">
              <a href="#users">
                <i class="fa fa-user icon">
                  <b class="bg-info"></b>
                </i>
                <span class="pull-right">
                  <i class="fa fa-angle-down text"></i>
                  <i class="fa fa-angle-up text-active"></i>
                </span>
                <span>User &amp; Group</span>
              </a>
              <ul class="nav lt">
                <li class="{{ Helper::set_active('user.user') }}">
                  <a href="{{ route('user.list') }}">
                    <i class="fa fa-angle-right"></i>
                    <span>User</span>
                  </a>
                </li>
                <li class="{{ Helper::set_active('user.group') }}">
                  <a href="{{ route('group.assign') }}">
                    <i class="fa fa-angle-right"></i>
                    <span>Group</span>
                  </a>
                </li>
              </ul>
            </li>
            
          </ul>
        </nav>
        
      </div>
    </section>
    
    <footer class="footer lt hidden-xs b-t b-dark">
      <a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-dark btn-icon">
        <i class="fa fa-angle-left text"></i>
        <i class="fa fa-angle-right text-active"></i>
      </a>
    </footer>
  </section>
</aside>