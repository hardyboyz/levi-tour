<div class="col-md-3 col-sm-3 col-xs-6">
	  <div class="alert alert-warning back-widget-set text-center">
			<a rel="levi-tour[gallery]" title="{{ $promotion->title }}" href="{!! URL::to('uploads/'.$promotion->image) !!}">
				<img alt="{{ $promotion->title }}" src="{!! URL::to('uploads/'.$promotion->image) !!}" height="100%" class="img-responsive"/>
			</a>
	</div>
</div>
 <div class="col-md-3 col-sm-3 col-xs-6">
	  <div class="alert alert-warning back-widget-set text-center">
		<header class="panel-heading bg-primary dker no-border"><strong>Calendar</strong></header>
		<div id="calendar" class="bg-primary m-l-n-xxs m-r-n-xxs"></div>
	</div>
</div>