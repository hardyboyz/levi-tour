<div class="m-b-md">
  	<!--<a href="{{ route('user.create') }}" id="btn-user-add" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> Add User</a>-->
  	<h3 class="m-b-none">List Reservation</h3>
</div>

{{ Helper::bootstrap_alert() }}

<section class="panel panel-default">
	<div class="table-responsive">
		<table class="table table-striped">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>Name</th>
	          <th>Phone </th>
	          <th>Email</th>
			  <th>Address</th>
			  {{-- <th>Package</th> --}}
			  <th>Total Pax</th>
			  <th>Submit Date</th>
			  <th>Arrival Date</th>
			  <th>Back Date</th>
			  <th>Wilayah</th>
	          <th class="col-md-2 text-center">Action</th>
	        </tr>
	      </thead>
	      <tbody>
	      	@foreach ($reservation as $key => $r)
	        <tr>
	          <th scope="row">{{ $key+1 }}</th>
	          <td>{{ $r->name }}</td>
	          <td>{{ $r->phone }} </td>
			  <td>{{ $r->email }} </td>
			  <td>{{ $r->address }} </td>
			  {{-- <td>{{ $r->packageName->title }} </td> --}}
			  <td>{{ $r->total_pax }} </td>
			  <td>{{ date('d-M-Y', strtotime($r->created_at)) }} </td>
			  <td>{{ date('d-M-Y', strtotime($r->arrival_date)) }} </td>
			  <td>{{ date('d-M-Y', strtotime($r->back_date)) }} </td>
			  <td>{{ $r->wilayah }} </td>
	          <td class="text-center">
                <a data-id="13" class="btn btn-xs btn-default btn-user-edit" href="{{ route('frontend.edit', $r->id) }}"><i class="fa fa-pencil fa-fw"></i></a>
	          </td>
	        </tr>
	        @endforeach
	      </tbody>
	    </table>
	</div>
</section>