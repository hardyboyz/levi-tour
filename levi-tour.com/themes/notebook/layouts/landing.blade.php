<!DOCTYPE html>
<html lang="en" class="">
<head>
  <meta charset="utf-8" />
  <title>{{ Theme::get('title') }}</title>
  <meta name="keywords" content="{{ Theme::get('keywords') }}">
  <meta name="description" content="{{ Theme::get('description') }}">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  {!! Theme::asset()->styles() !!}
  {!! Theme::asset()->container('post-styles')->styles() !!}
  {!! Theme::asset()->scripts() !!}
  <!--[if lt IE 9]>
    {!! Theme::asset()->container('ie9')->scripts() !!}
  <![endif]-->
</head>
<body>
  	
  <!-- header -->
  <header id="header" class="navbar navbar-fixed-top bg-white box-shadow b-b b-light"  data-spy="affix" data-offset-top="1">
    <div class="container">
      <div class="navbar-header">        
        <a href="#" class="navbar-brand"><img src="{!! Theme::asset()->url('img/logo.jpg') !!}" class="m-r-sm"><span class="text-muted">We know what you want</span></a>
        <button class="btn btn-link visible-xs" type="button" data-toggle="collapse" data-target=".navbar-collapse">
          <i class="fa fa-bars"></i>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li class="{{ Helper::set_active('home.index') }}">
            <a href="{{ route('home') }}"> 
				<i class="fa fa-home icon">
                  <b class="bg-warning"></b>
                </i>
				Home</a>
          </li>
          <li class="{{ Helper::set_active('profile') }}">
            <a href="{{ route('profile') }}">
				<i class="fa fa-user icon">
                  <b class="bg-warning"></b>
                </i>
				Profile</a>
          </li>
          <li class="{{ Helper::set_active('services') }}">
            <a href="{{ route('services') }}">
				<i class="fa fa-book icon">
                  <b class="bg-warning"></b>
                </i>
				Services</a>
          </li>
		  <li class="{{ Helper::set_active('gallery') }}">
            <a href="{{ route('gallery') }}">
				<i class="fa fa-image icon">
                  <b class="bg-warning"></b>
                </i>
				Gallery</a>
          </li>
		  <li class="{{ Helper::set_active('package') }}">
            <a href="{{ route('listPackage') }}">
			<i class="fa fa-cogs icon">
                  <b class="bg-warning"></b>
                </i>
				Package</a>
          </li>
		  <li class="{{ Helper::set_active('reservation') }}">
            <a href="{{ route('reservation') }}">
				<i class="fa fa-calendar icon">
                  <b class="bg-warning"></b>
                </i>
				Reservation</a>
          </li>
          <!--<li>
            <div class="m-t-sm">
              <a href="{{ route('_auth.login') }}" class="btn btn-link btn-sm">Sign in</a>
              <a href="#" class="btn btn-sm btn-success m-l"><strong>Sign up</strong></a>
            </div>
          </li>-->
        </ul>
      </div>
    </div>
  </header>
  <!-- / header -->
	<section id="content">
	  {!! Theme::content() !!}
	</section>
  
  <!-- footer -->
  <footer id="footer">
    <div class="bg-dark dker wrapper">
      <div class="container text-center m-t-lg">
        <div class="row m-t-xl m-b-xl">
          <div class="col-sm-4" data-ride="animated" data-animation="fadeInLeft" data-delay="300">
            <i class="fa fa-map-marker fa-3x icon-muted"></i>
            <h5 class="text-uc m-b m-t-lg">Find Us</h5>
            <p class="text-sm">Jl. Telex Air Ketekok, Tanjungpandan Belitung <br>
              Bangka Belitung Island 0812-7194-066 
             </p>
          </div>
          <div class="col-sm-4" data-ride="animated" data-animation="fadeInUp" data-delay="600">
            <i class="fa fa-envelope-o fa-3x icon-muted"></i>
            <h5 class="text-uc m-b m-t-lg">Mail Us</h5>
            <p class="text-sm"><a href="mailto:>levi_tour@yahoo.co.id">levi_tour@yahoo.co.id</a></p>
          </div>
          <div class="col-sm-4" data-ride="animated" data-animation="fadeInRight" data-delay="900">
            <i class="fa fa-globe fa-3x icon-muted"></i>
            <h5 class="text-uc m-b m-t-lg">Tour with Us</h5>
            <p class="text-sm">Send your <a href="{{route ('reservation') }}">Reservation</a></p>
          </div>
        </div>
        <div class="m-t-xl m-b-xl">
          <p>
            <a href="https://www.facebook.com/groups/315022331867504/" target="_blank" class="btn btn-icon btn-rounded btn-facebook bg-empty m-sm"><i class="fa fa-facebook"></i></a>
            <a href="#" class="btn btn-icon btn-rounded btn-twitter bg-empty m-sm"><i class="fa fa-twitter"></i></a>
            <a href="#" class="btn btn-icon btn-rounded btn-gplus bg-empty m-sm"><i class="fa fa-google-plus"></i></a>
          </p>
          <p>
            <a href="#content" data-jump="true" class="btn btn-icon btn-rounded btn-dark b-dark bg-empty m-sm text-muted"><i class="fa fa-angle-up"></i></a>
          </p>
        </div>
      </div>
    </div>
  </footer>
  <!-- / footer -->  
  {!! Theme::asset()->container('core-scripts')->scripts() !!}
  {!! Theme::asset()->container('post-scripts')->scripts() !!}

</body>
</html>