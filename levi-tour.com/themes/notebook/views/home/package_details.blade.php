<!-- LOGO HEADER END-->
    <section class="menu-section">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		   <!--  <a class="navbar-brand" href="index.html">
				<img src="assets/img/logo.png" />
			</a> -->

		</div>

	   <!--  <div class="right-div">
			<a href="#" class="btn btn-danger pull-right">LOG ME OUT</a>
		</div> -->
	</div>
    </section>
     <!-- MENU SECTION END-->
    <div class="content-wrapper">
	  
         <div class="container">
                         
             <div class="row" style="margin-top:10px">
              <div class="col-md-9 col-sm-9 col-xs-12" style="min-height:400px">
				<div class="row">
					<div class="panel panel-warning">
						<div class="panel-heading">
						<h4 class="title">{!! $package->title !!}</h4>
						</div>
						<div class="panel-body">
							@if(strlen($package->image) > 0)
							<a rel="levi-tour[gallery]" title="{{ $package->title }}" href="{!! URL::to('uploads/'.$package->image) !!}">
							<img src="{!! URL::to('uploads/'.$package->image) !!}" height="170px" class="pull-left">
							</a>
							@else
							<img src="/themes/notebook/assets/img/levi-tour.jpg" alt="Levi Tour Belitung"  height="250px" class="pull-left"/>
							@endif
							<div class="content">
							<!--{!! $package->short_details !!} <br />-->
							{!! $package->description !!}
							</div>
						</div>
						<div class="panel-footer">
						
						</div>
					</div>
				</div>
			  
			</div>
            
			
        <div class="row">
            
              <div class="col-md-3 col-sm-3 col-xs-6">
				  <div class="alert alert-warning back-widget-set text-center">
						<a rel="levi-tour[gallery]" title="{{ $promotion->title }}" href="{!! URL::to('uploads/'.$promotion->image) !!}">
							<img alt="{{ $promotion->title }}" src="{!! URL::to('uploads/'.$promotion->image) !!}" height="100%" class="img-responsive"/>
						</a>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-6">
				  <div class="alert alert-warning back-widget-set text-center">
						<div class="panel-heading">
							Supported By
						</div>
						<div class="panel-body">
							<img src="/themes/notebook/assets/img/1.jpg" alt="" class="img-responsive"/>
						</div>
				</div>
			</div>
        </div>  

    </div>
	<script type="text/javascript" charset="utf-8">
	   $(document).ready(function(){
		 $("a[rel^='levi-tour']").prettyPhoto();
	   });
	 </script>