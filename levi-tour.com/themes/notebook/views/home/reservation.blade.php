
     <!-- MENU SECTION END-->
    <div class="content-wrapper">
	  
	 
         <div class="container">
                         
             <div class="row" style="margin-top:60px;">

				<div class="col-md-9 col-sm-9 col-xs-12">
				{{ Helper::bootstrap_alert() }}
					<div class="panel panel-default">
						<div class="panel-heading">
						<h3>Reservation</h3>
						</div>
						<div class="panel-body">
							 {!! Form::open(array('route' => 'reservation.post')) !!}
						
							 <div class="form-group">
								<label>Arrival Date</label>
								{!! Form::text('arrival_date', '', array('class' => 'form-control','id'=>'arrival_date')) !!}
								
							  </div>
							 <div class="form-group">
								<label>Choose Package</label>
								{!! Form::select('package', $packages,'', array('class' => 'form-control')) !!}
							  </div>
							<div class="form-group">
								<label for="name" class="cols-sm-2 control-label">Total Pax</label>
								<div class="cols-sm-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
										<input type="number" class="form-control" name="total_pax" id="pax"  placeholder="Total Pax" min="1" value="1" required="true"/>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<label for="name" class="cols-sm-2 control-label">Your Name</label>
								<div class="cols-sm-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
										<input type="text" class="form-control" name="name" id="name"  placeholder="Enter your Name" required="true"/>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<label for="phone" class="cols-sm-2 control-label">Your Phone</label>
								<div class="cols-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-phone fa" aria-hidden="true"></i></span>
										<input type="text" class="form-control" name="phone" id="phone"  placeholder="Enter your phone" required="true"/>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<label for="email" class="cols-sm-2 control-label">Your Email</label>
								<div class="cols-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
										<input type="text" class="form-control" name="email" id="email"  placeholder="Enter your Email" required="true"/>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<label for="address" class="cols-sm-2 control-label">Address</label>
								<div class="cols-sm-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
										<input type="text" class="form-control" name="address" id="address"  placeholder="Enter your address" required="true"/></textarea>
									</div>
								</div>
							</div>

							<div class="form-group ">
								<button type="submit" id="button" class="btn btn-primary btn-lg btn-block login-button">Submit Reservation</button>
							</div>
							
						</form>
						</div>
						<div class="panel-footer">
							Please complete the form correctly.
						</div>
					</div>
				</div>
			
            
			
        <div class="row">
            
              <div class="col-md-3 col-sm-3 col-xs-6">
				  <div class="alert alert-info back-widget-set text-center">
						<a rel="levi-tour[gallery]" title="{{ $promotion->title }}" href="{!! URL::to('uploads/'.$promotion->image) !!}">
							<img alt="{{ $promotion->title }}" src="{!! URL::to('uploads/'.$promotion->image) !!}" height="100%" class="img-responsive"/>
						</a>
				</div>
			</div>
        </div> 

    </div>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#arrival_date").datetimepicker(
		{
			inline: true,
            sideBySide: true,
			minDate: new Date()
		});
		
	});
	</script>  
	
	<script type="text/javascript" charset="utf-8">
	   $(document).ready(function(){
		 $("a[rel^='levi-tour']").prettyPhoto();
	   });
	 </script>